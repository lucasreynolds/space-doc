# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ProgrammingWebsite::Application.config.secret_key_base = 'b7fcfe6cd475156cdedadf4f7cfd7fb284ecd41ef18612446d6b0f7b79ea1deb04a6a11273f0965efa2accf7212d22ca57e541c0667e9154309a23afdd567eac'

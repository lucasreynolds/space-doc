class PagesController < ApplicationController
  
   def new
   end
   
   def create
     @page = Page.new(params.require(:page).permit(:title, :text))
     @page.save

     redirect_to @page
   end

   def show
     @page = Page.find(params[:id])
   end

   def index
     @page = Page.all
   end

   def edit
     @page = Page.find(params[:id])
   end
   
   def update
     @page = Page.find(params[:id])
     
     if @page.update(params.require(:page).permit(:title, :text))
       redirect_to @page
     else
       render 'edit'
     end
   end

   def destroy
     @page = Page.find(params[:id])
     @page.destroy

     redirect_to pages_path
   end 
end
